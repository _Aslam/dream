Abstract: To ensure Cyber security of an organization, we can leverage Deep learning (Recurrent Neural Networks) to act upon the system exploit,
threats or breaches like intrusion detection, malware detection, spam and phishing detection that are taking place in the digital world. Deep learning
is the subversion of machine learning,and machine learning is a subversion of artificial intelligence. Since Deep learning is more advanced and has 
higher forecasting value and the prediction can be improved continuously. Since the amount of data is enormous (big data) we can use that data to train
our model. This paper outlines the survey of all the works related to Deep learning based solutions for various Cyber security use-cases.

Keywords: Cyber security, Deep learning, machine learning, Recurrent Neural Networks, Big data, intrusion detection ,malware detection, spam and phishing detection.
